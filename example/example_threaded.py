import threading
import time

from pixelink import PixeLINK, PxLerror


def grab_frames(cam):
    frame_num = 0
    time_start = time.time()
    print('Continuous frame grabbing started...')
    while cam.is_open():
        frame_num += 1
        try:
            data = cam.grab()
            # TODO: do something with the data...
        except PxLerror as exc:
            print('ERROR: grab_frames:', str(exc))
            continue
        t_total = time.time() - time_start
        if frame_num % 10 == 0:
            frame_rate = float(frame_num) / t_total
            print('#%04d FPS: %0.3f frames/sec' % (frame_num, frame_rate))


def main():
    cam = PixeLINK()
    cam.shutter = 0.002  # exposure time in seconds
    th = threading.Thread(target=grab_frames, args=[cam])
    th.start()
    try:
        while True:
            time.sleep(1.0)
    except KeyboardInterrupt:
        print('Caught CTRL+C')
    finally:
        print('Closing camera...')
        cam.close()
        print('Waiting for thread...')
        th.join()
        print('Done.')


if __name__ == '__main__':
    main()
